#!/usr/bin/env ruby

class FibonacciPerimeter
  @n = 1

  def initialize(n)
    if !n.kind_of?(Integer)
      raise ArgumentError, 'n must be an Integer!'
    end

    @n = n + 1
  end

  def get_perimeter
    if @n < 0
      raise ArgumentError 'n must be >= 0'
    end

    last = 0
    before_last = 0
    result = 0

    (0..@n).each do |num|
      current = last + before_last
      if num == 0
        last = 1
      end

      before_last = last
      last = current

      result += current
    end

    return result * 4
  end

  private

  def get_fibonacci(n)
    if n == 0
      return 0
    elsif n == 1
      return 1
    else
      return get_fibonacci(n - 1) + get_fibonacci(n - 2)
    end
  end

end

def perimeter(n)
  begin
    fp = FibonacciPerimeter.new(n)
    return fp.get_perimeter()
  rescue
    # handle errors if needed!
  end

  return 0
end

if __FILE__ == $0
  puts perimeter(500000)
end

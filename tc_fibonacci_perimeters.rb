#!/usr/bin/env ruby

require_relative "fibonacci_perimeters"
require "test/unit"


class TestValidFibPer < Test::Unit::TestCase
  def test_valid_nil
    assert_equal(0, perimeter(nil))
  end

  def test_valid_variable
    assert_equal(0, perimeter('a'))
  end

  def test_valid_array
    assert_equal(0, perimeter([]))
  end

  def test_float_number
    assert_equal(0, perimeter(4.2))
  end

  def test_negativ_number
    assert_equal(0, perimeter(-3))
  end

  def test_num_1
    assert_equal(8, perimeter(1))
  end

  def test_num_5
    assert_equal(80, perimeter(5))
  end

  def test_num_7
    assert_equal(216, perimeter(7))
  end

  def test_num_20
    resp = 114624
    assert_equal(resp, perimeter(20))
  end

  def test_num_100
    assert_equal(perimeter(100), 6002082144827584333104)
  end
end
